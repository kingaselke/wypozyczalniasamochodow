﻿using System;
using System.Linq;
using CarRentalProjectKM.BusinessLayer.Dtos;
using CarRentalProjectKM.BusinessLayer.Services;
using CarRentalProjectKM.Helpers;

namespace CarRentalProjectKM
{
    internal class ProgramLoop
    {

        public void Execute()
        {
            var exit = false;
            while (!exit)
            {
                Console.WriteLine("WYPOŻYCZALNIA SAMOCHODÓW\n\n");
                var choice = ConsoleReadHelper.GetCommand("Wybierz czynność: \n");

                switch (choice)
                {
                    case "1":
                        AddCarModel();
                        break;
                    case "2":
                        AddSpecyficCar();
                        break;
                    case "3":
                        AddCustomers();
                        break;
                    case "4":
                        RentCar();
                        break;
                    case "5":
                        ReturnCar();
                        break;
                    case "6":
                        AddDiscount();
                        break;
                    case "7":
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Wprowadzone polecenie: '" +
                                          choice + "' jest nieprawidłowe. Spróbuj ponownie");
                        break;
                }
            }
        }

        private static void AddCarModel()
        {
            Console.WriteLine("Wprowadż informacje o samochodzie:\n ");

            var car = new CarModelDto();
            car.Brand = ConsoleReadHelper.GetNameOfSmth("Podaj markę: ");
            car.Model = ConsoleReadHelper.GetNameOfSmth("Podaj model: ");
            car.PricePerDay = ConsoleReadHelper.GetDouble("Podaj cenę: ");

            var success = false;
            try
            {
                var carService = new CarModelService();
                success = carService.AddCarModel(car);
            }
            catch (Exception e)
            {
                Console.WriteLine("Błąd przy dodawaniu samochodu: " + e.Message);
            }

            ConsoleWriteHelper.PrintOperationSuccessMessage(success);
        }

        private static void AddSpecyficCar()
        {
            var specyficCar = new CarDto();
            var carsService = new CarService();

            if (carsService.GettAllCarModels().Count == 0 || carsService.GettAllCarModels() == null)
            {
                Console.WriteLine("Brak dodanych modeli samochodów do bazy.\n" +
                                  "Jeżeli chcesz dodać wybrany pojazd najpierw dodaj model samochodu!\n");
                return;
            }

            Console.WriteLine("Wprowadż informacje o konkretnym samochodzie:\n ");
            var work = true;
            while (work)
            {
                Console.WriteLine("Wybierz model (wpisz nazwę modelu) lub 'exit' aby powrócić do głównego menu: ");

                foreach (var cars in carsService.GettAllCarModels())
                {
                    Console.WriteLine(cars.Brand + " " + cars.Model);
                }
                var model = Console.ReadLine();

                if (model == "exit")
                {
                    return;
                }
                if (carsService.GettAllCarModels().Where(x => x.Model == model).ToList().Count == 0)
                {
                    Console.WriteLine("Brak wybranego modelu w bazie.");
                }
                else
                {
                    specyficCar.CarModelDto = carsService.GettAllCarModels().First(x => x.Model == model);
                    work = false;
                }
            }

            var exit = false;
            while (!exit)
            {
                var plate = ConsoleReadHelper.GetNameOfSmth("Podaj numer rejestracyjny: ");
                if (carsService.GetAllCars().Where(x => x.Plate == plate).ToList().Count == 0)
                {
                    specyficCar.Plate = plate;
                    exit = true;
                }
                else
                {
                    Console.WriteLine("Samochód o podanym numerze rejestracyjnym istnieje w bazie. Spróbuj ponownie!");
                }
            }
            specyficCar.RentOrNotRent = true;
            var success = false;

            try
            {
                var carService = new CarService();
                success = carService.AddSpecyficCar(specyficCar);
            }
            catch (Exception e)
            {
                Console.WriteLine("Błąd przy dodawaniu samochodu: " + e.Message);
            }

            ConsoleWriteHelper.PrintOperationSuccessMessage(success);
        }
        private static void AddCustomers()
        {
            Console.WriteLine("Wprowadż dane osobowe klienta:\n ");

            var customer = new CustomerDto();

            var exit = false;
            while (!exit)
            {
                var pesel = ConsoleReadHelper.GetLong("Podaj PESEL: ");
                var customerService = new CustomerService();
                if (!customerService.CheckIfCustomerExistsInTheSystemByPesel(pesel))
                {
                    customer.Pesel = pesel;
                    exit = true;
                }
                else
                {
                    Console.WriteLine("Klient o podanym peselu istnieje w bazie danych! Spróbuj ponownie!");
                }
            }

            customer.Name = ConsoleReadHelper.GetNameOfSmth("Podaj imię: ");
            customer.Surname = ConsoleReadHelper.GetNameOfSmth("Podaj nazwisko: ");

            var success = false;
            try
            {
                var customerService = new CustomerService();
                success = customerService.AddCustomers(customer);
            }
            catch (Exception e)
            {
                Console.WriteLine("Błąd przy dodawaniu klienta: " + e.Message);
            }
            ConsoleWriteHelper.PrintOperationSuccessMessage(success);
        }

        private static void RentCar()
        {
            var rentCarService = new RentCarService();
            var allCustomers = rentCarService.GetAllCustomers();
            var availableCars = rentCarService.GetAvailableCars();
            if ((allCustomers == null || allCustomers.Count == 0) &&
                (availableCars == null || availableCars.Count == 0))
            {
                Console.WriteLine("Brak dodanych klientów oraz samochodów do bazy. Dodaj klienta oraz samochód," +
                                  "aby wypożyczyć.\n\n");
                return;
            }
            if (allCustomers == null || allCustomers.Count == 0)
            {
                Console.WriteLine("Brak dodanych klientów do bazy. Dodaj klienta, aby wypozyczyć.\n\n");
                return;
            }
            if (availableCars == null || availableCars.Count == 0)
            {
                Console.WriteLine("Brak dodanych samochodów do bazy. Dodaj samochód, aby wypożyczyć. \n\n");
                return;
            }

            Console.WriteLine("Wybierz klienta:");

            foreach (var customer in allCustomers)
            {
                Console.WriteLine(customer.Pesel + " " + customer.Name +
                    " " + customer.Surname);
            }
            var rentCar = new RentCarDto();
            var done = false;
            while (!done)
            {
                var selectedCustomer = ConsoleReadHelper.GetLong("\n\nPodaj pesel:");
                if (allCustomers.Where(x => x.Pesel == selectedCustomer).ToList().Count == 0)
                {
                    Console.WriteLine("Brak klienta o podanym peselu. Spróbuj ponownie");
                }
                else
                {
                    rentCar.CustomerDto = allCustomers.First(x => x.Pesel == selectedCustomer);
                    done = true;
                }
            }
            Console.WriteLine("Wybierz samochód z listy " +
                              "dostępnych samochodów (Podaj nr rejestracyjny): ");

            foreach (var car in availableCars)
            {
                Console.WriteLine(car.CarModelDto.Model + car.Plate);
            }
            var done1 = false;
            while (!done1)
            {
                var selectedCar = Console.ReadLine();
                if (availableCars.Where(x => x.Plate == selectedCar).ToList().Count == 0)
                {
                    Console.WriteLine("Brak samochodu o podanej tablicy rejestracyjnej." +
                                      "Spróbuj ponownie. Podaj nr rejestracyjny:");
                }
                else
                {
                    rentCar.CarDto = availableCars.First(x => x.Plate == selectedCar);
                    done1 = true;
                }
            }
            rentCar.RentDate = ConsoleReadHelper.GetDate("Podaj datę wypożyczenia:");
            rentCar.CarDto.RentOrNotRent = false;
            var success = false;

            try
            {
                success = rentCarService.AddRental(rentCar);
            }
            catch (Exception e)
            {
                Console.WriteLine("Błąd przy dodawaniu klienta: " + e.Message);
            }

            ConsoleWriteHelper.PrintOperationSuccessMessage(success);
        }

        private static void ReturnCar()
        {
            var returnCarService = new ReturnCarService();
            var rentedCars = returnCarService.GetRentedCars();
            if (rentedCars == null || rentedCars.Count == 0)
            {
                Console.WriteLine("Brak wypożyczonych samochodów.\n");
                return;
            }

            Console.WriteLine("Wybierz samochoód do zwrotu (Podaj numer rejestracyjny):");

            foreach (var car in rentedCars)
            {
                Console.WriteLine(car.Plate + " " + car.CarModelDto.Brand + " " + car.CarModelDto.Model);
            }
            var done = false;
            while (!done)
            {
                var selectedCar = Console.ReadLine();
                if (rentedCars.Where(x => x.Plate == selectedCar).ToList().Count == 0)
                {
                    Console.WriteLine("Brak wypożyczonego samochodu o podanym numerze rejestracyjnym. Podaj poprawny" +
                                      "numer rejestracyjny!");
                }
                foreach (var car in rentedCars)
                {
                    if (car.Plate == selectedCar)
                    {
                        var rental = returnCarService.GetActiveRental(selectedCar);
                        returnCarService.ChangeRentOrNotRent(selectedCar);
                        returnCarService.ChangeReturnDate(DateTime.Today, rental.Id);
                        var days = DateTime.Today - rental.RentDate;
                        Console.WriteLine("Liczba dni wypożyczenia: " + days);
                        var totalPrice =
                            returnCarService.GetTotalPrice(days.Days, rental.CarDto.CarModelDto.PricePerDay, rental.CustomerDto.Pesel);
                        Console.WriteLine("Kwota do zapłaty: " + totalPrice);
                        returnCarService.ChangeTotalPrice(totalPrice, rental.Id);
                        var availableDiscount = DiscountService.GettAllDiscounts();
                        foreach (var discounts in availableDiscount)
                        {
                            Console.WriteLine(discounts.Name);
                        }

                        var done1 = false;
                        while (!done1)
                        {
                            Console.WriteLine("Podaj jaką promocję chcesz dodać (wpisz 'none' by nie korzystać z promocji):");
                            var selectedDoiscount = Console.ReadLine();
                            if (selectedDoiscount == "none")
                            {
                                return;
                            }
                            foreach (var discounts in availableDiscount)
                            {
                                if (discounts.Name == selectedDoiscount)
                                {
                                    var totalCost = new DiscountService().TotalPrice(totalPrice, discounts);
                                    Console.WriteLine("Total cost: " + totalCost);
                                    returnCarService.ChangeTotalPrice(totalCost, rental.Id);
                                    done1 = true;
                                }
                                else
                                {
                                    Console.WriteLine("Brak promocji o podanej nazwie. Spróbuj ponownie.");
                                }
                            }
                        }
                        done = true;
                    }
                }
            }
        }

        private static void AddDiscount()
        {
            var discount = new DiscountDto();

           discount.Name = ConsoleReadHelper.GetNameOfSmth("Podaj nazwę promocji: ");
            discount.DiscountPercentage = ConsoleReadHelper.GetDouble("Podaj wartość zniżki [%]: ");

            var success = false;
            try
            {
                var discountService = new DiscountService();
                success = discountService.AddDiscount(discount);
            }
            catch (Exception e)
            {
                Console.WriteLine("Błąd przy dodawaniu promocji: " + e.Message);
            }
            ConsoleWriteHelper.PrintOperationSuccessMessage(success);
        }
    }
}