﻿using System;
using CarRentalProjectKM.BusinessLayer.Services;

namespace CarRentalProjectKM.Helpers
{
    internal class ConsoleReadHelper
    {
        public static string GetCommand(string message)
        {
            Console.Write(message + "\n 1. Dodaj model pojazdu.\n 2. Dodaj wybrany pojazd.\n" +
                          " 3. Dodaj klienta.\n 4. Wypożysz samochód.\n 5. Zwróć samochód\n " +
                          "6. Dodaj promocję.\n 7. Exit.\n\n");
            var choice = Console.ReadLine();
            return choice;
        }

        public static long GetLong(string message)
        {
            long pesel;
            Console.Write(message);

            while (!long.TryParse(Console.ReadLine(), out pesel))
            {
                Console.WriteLine("Podano błędną wartość - spróbuj ponownie!");
            }

            return pesel;
        }

        public static int GetInt(string message)
        {
            int number;
            Console.Write(message);

            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Wartość musi być liczbą - spróbuj ponownie!");
            }

            return number;
        }

        public static double GetDouble(string message)
        {
            double number;
            Console.Write(message);

            while (!double.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Podano błędną wartość - spróbuj ponownie!");
            }

            return number;
        }

        public static DateTime GetDate(string message)
        {
            DateTime date;
            Console.Write(message);

            while (!DateTime.TryParse(Console.ReadLine(), out date))
            {
                Console.WriteLine("Zły format daty - spróbuj ponownie! (dd/mm/yyyy");
            }

            return date;
        }

        public static string GetNameOfSmth(string massage)
        {
            string nameOfSmth = null;
            var exit = false;
            while (!exit)
            {
                Console.WriteLine(massage);
                nameOfSmth = Console.ReadLine();
                if (String.IsNullOrWhiteSpace(nameOfSmth))
                {
                    Console.WriteLine("Wartość wymagana. Uzupełnij pole.");
                }
                else
                {
                    exit = true;
                }
            }
            return nameOfSmth;
        }
    }
}

//var exit = false;
//while (!exit)
//{
//Console.WriteLine("Podaj markę: ");
//car.Brand = Console.ReadLine();
//if (String.IsNullOrWhiteSpace(car.Brand))
//{
//    Console.WriteLine("Wartość wymagana. Uzupełnij nazwę marki.");
//}
//else
//{
//exit = true;
//}
//}
