﻿using System;

namespace CarRentalProjectKM.Helpers
{
    internal class ConsoleWriteHelper
    {
        public static void PrintOperationSuccessMessage(bool success)
        {
            Console.WriteLine(success ? "\nOperacja wykonana pomyślnie!\n" : "\nOperacja zakończona niepowodzeniem!\n");
        }
    }
}
