﻿using CarRentalProjectKM.BusinessLayer.Dtos;
using CarRentalProjectKM.DataLayer.Models;

namespace CarRentalProjectKM.BusinessLayer.Mappers
{
    internal class DtoToEntityMappers
    {
        public static CarModel CarModelDtoToEntityModel(CarModelDto carModelDto)
        {
            if (carModelDto == null)
            {
                return null;
            }

            var car = new CarModel()
            {
                Id = carModelDto.Id,
                Brand = carModelDto.Brand,
                Model = carModelDto.Model,
                PricePerDay = carModelDto.PricePerDay
            };
            return car;
        }

        public static Discount DiscountDtoToEntityModel(DiscountDto discountDto)
        {
            if (discountDto == null)
            {
                return null;
            }

            var discount = new Discount
            {
                Id = discountDto.Id,
                Name = discountDto.Name,
                DiscountPercentage = discountDto.DiscountPercentage
            };

            return discount;
        }

        public static Car CarDtoToEntityModel(CarDto carDto)
        {
            if (carDto == null)
            {
                return null;
            }

            var car = new Car
            {
                Id = carDto.Id,
                Plate = carDto.Plate,
                RentOrNotRent = carDto.RentOrNotRent,
                CarModel = CarModelDtoToEntityModel(carDto.CarModelDto)
            };
            return car;
        }

        public static Customer CustomerDtoToEntityModel(CustomerDto customerDto)
        {
            if (customerDto == null)
            {
                return null;
            }
            var customer = new Customer
            {
                Pesel = customerDto.Pesel,
                Name = customerDto.Name,
                Surname = customerDto.Surname
            };
            return customer;
        }

        public static RentCar RentCarDtoToEntityModel(RentCarDto rentCarDto)
        {
            if (rentCarDto == null)
            {
                return null;
            }

            var rentCar = new RentCar
            {
                Id = rentCarDto.Id,
                RentDate = rentCarDto.RentDate,
                Customer = CustomerDtoToEntityModel(rentCarDto.CustomerDto),
                Car = CarDtoToEntityModel(rentCarDto.CarDto)
            };
            return rentCar;
        }
    }
}
