﻿using CarRentalProjectKM.BusinessLayer.Dtos;
using CarRentalProjectKM.DataLayer.Models;

namespace CarRentalProjectKM.BusinessLayer.Mappers
{
    internal class EntityToDtoMapper
    {
        public static CustomerDto CustomerEntityModelToDto(Customer customer)
        {
            if (customer == null)
            {
                return null;
            }

            var customerDto = new CustomerDto()
            {
                Pesel = customer.Pesel,
                Name = customer.Name,
                Surname = customer.Surname
            };
            return customerDto;
        }

        public static CarModelDto CarModelEntityModelToDto(CarModel carModel)
        {
            if (carModel == null)
            {
                return null;
            }

            var car = new CarModelDto()
            {
                Id = carModel.Id,
                Brand = carModel.Brand,
                Model = carModel.Model,
                PricePerDay = carModel.PricePerDay
            };
            return car;
        }
        public static CarDto CarEntityModelToDto(Car car)
        {
            if (car == null)
            {
                return null;
            }

            var carDto = new CarDto()
            {
                Id = car.Id,
                Plate = car.Plate,
                RentOrNotRent = car.RentOrNotRent,
                CarModelDto = CarModelEntityModelToDto(car.CarModel)
            };
            return carDto;
        }
        public static DiscountDto DiscountEntityModelToDto(Discount discount)
        {
            if (discount == null)
            {
                return null;
            }

            var discountDto = new DiscountDto()
            {
                Id = discount.Id,
                Name = discount.Name,
                DiscountPercentage = discount.DiscountPercentage
            };
            return discountDto;
        }

        public static RentCarDto RentCarEntityModelToDto(RentCar rentCar)
        {
            var rentCarDto = new RentCarDto()
            {
                Id = rentCar.Id,
                CustomerDto = CustomerEntityModelToDto(rentCar.Customer),
                CarDto = CarEntityModelToDto(rentCar.Car),
                RentDate = rentCar.RentDate,
                ReturnDate = rentCar.ReturnDate,
                TotalPrice = rentCar.TotalPrice
            };
            return rentCarDto;
        }
    }
}
