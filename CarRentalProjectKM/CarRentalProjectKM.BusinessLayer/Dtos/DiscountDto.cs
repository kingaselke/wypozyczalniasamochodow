﻿
namespace CarRentalProjectKM.BusinessLayer.Dtos
{
   public class DiscountDto
   {
       public int Id;
       public string Name;
       public double DiscountPercentage;
   }
}
