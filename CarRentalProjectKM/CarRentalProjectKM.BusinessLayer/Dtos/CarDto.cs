﻿
namespace CarRentalProjectKM.BusinessLayer.Dtos
{
    public class CarDto
    {
        public int Id;
        public string Plate;
        public bool RentOrNotRent;
        public CarModelDto CarModelDto;
    }
}
