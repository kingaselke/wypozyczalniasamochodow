﻿using System;

namespace CarRentalProjectKM.BusinessLayer.Dtos
{
    public class RentCarDto
    {
        public int Id;
        public DateTime RentDate;
        public DateTime? ReturnDate;
        public CustomerDto CustomerDto;
        public CarDto CarDto;
        public double? TotalPrice;
    }
}
