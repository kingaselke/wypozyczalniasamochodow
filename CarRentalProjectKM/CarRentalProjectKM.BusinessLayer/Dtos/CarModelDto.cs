﻿
namespace CarRentalProjectKM.BusinessLayer.Dtos
{
    public class CarModelDto
    {
        public int Id;
        public string Brand;
        public string Model;
        public double PricePerDay;
    }
}
