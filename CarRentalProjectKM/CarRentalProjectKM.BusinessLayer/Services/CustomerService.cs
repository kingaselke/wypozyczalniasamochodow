﻿using CarRentalProjectKM.DataLayer.Repositories;
using System;
using CarRentalProjectKM.BusinessLayer.Dtos;
using CarRentalProjectKM.BusinessLayer.Mappers;

namespace CarRentalProjectKM.BusinessLayer.Services
{
    public class CustomerService
    {
        public bool CheckIfCustomerExistsInTheSystemByPesel(long pesel)
        {
            var customersRepository = new CustomersRepository();
            var customer = customersRepository.GetCustomerByPesel(pesel);

            return customer != null && customer.Count != 0;
        }

        public bool AddCustomers(CustomerDto customerDto)
        {
            if (CheckIfCustomerExistsInTheSystemByPesel(customerDto.Pesel))
            {
                throw new Exception("Klient już istnieje w bazie danych!");
            }

            var customer = DtoToEntityMappers.CustomerDtoToEntityModel(customerDto);
            var customersRepository = new CustomersRepository();
            return customersRepository.AddCustomer(customer);
        }
        public CustomerDto GetCustomerByPesel(long pesel)
        {
            var customerRepository = new CustomersRepository();
            var customers = customerRepository.GetCustomerByPesel(pesel);
            var customerDto = new CustomerDto();
            foreach (var customer in customers)
            {
                if (customer.Pesel == pesel)
                {
                    customerDto = EntityToDtoMapper.CustomerEntityModelToDto(customer);
                }
            }
            return customerDto;
        }

        public void ChangeCustomerData(CustomerDto customerDto)
        {
            var customerRepository = new CustomersRepository();
            customerRepository.ChangeCustomerRepository(DtoToEntityMappers.CustomerDtoToEntityModel(customerDto));
        }
    }
}
