﻿using CarRentalProjectKM.BusinessLayer.Dtos;
using System;
using System.Collections.Generic;
using CarRentalProjectKM.BusinessLayer.Mappers;
using CarRentalProjectKM.DataLayer.Repositories;

namespace CarRentalProjectKM.BusinessLayer.Services
{
    public class ReturnCarService
    {
        public List<CarDto> GetRentedCars()
        {
            var rentedCars = new List<CarDto>();
            var rentCarRepository = new RentCarRepository();
            var allCars = rentCarRepository.GetAllCars();
            foreach (var car in allCars)
            {
                if (car.RentOrNotRent) continue;
                var carDto = EntityToDtoMapper.CarEntityModelToDto(car);
                rentedCars.Add(carDto);
            }
            return rentedCars;
        }

        public void ChangeRentOrNotRent(string plate)
        {
            var carRepository = new CarRepository();
            carRepository.ChangeRentOrNotRent(plate);
        }

        public void ChangeReturnDate(DateTime date, int id)
        {
            var rentCarRepository = new RentCarRepository();
            rentCarRepository.ChangeReturnDate(date, id);
        }

        public void ChangeTotalPrice(double price, int id)
        {
            var rentCarRepository = new RentCarRepository();
            rentCarRepository.ChangeTotalPrice(price, id);
        }

        public RentCarDto GetActiveRental(string plate)
        {
            var rentCarRepository = new RentCarRepository();
            var carRentals = rentCarRepository.GetAllRentalsByPlate(plate);
            var rentCarDto = new RentCarDto();
            foreach (var rental in carRentals)
            {
                if (rental.Car.RentOrNotRent == false)
                {
                    rentCarDto = EntityToDtoMapper.RentCarEntityModelToDto(rental);
                }
            }
            return rentCarDto;
        }

        public double GetTotalPrice(int numbersOfDays, double pricePerDay, long pesel)
        {
            var rentCarRepository = new RentCarRepository();
            var rentCarsByPerson = rentCarRepository.GetAllRentalsByPesel(pesel);
            double totalPrice;
            if (rentCarsByPerson.Count >= 10)
            {
                var price = numbersOfDays * pricePerDay;
                totalPrice = price - 1.0d / 10.0d * price;
            }
            else
            {
                totalPrice = numbersOfDays * pricePerDay;
            }
            return totalPrice;
        }
    }
}
