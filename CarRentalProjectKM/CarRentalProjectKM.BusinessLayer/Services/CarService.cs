﻿using System.Collections.Generic;
using System.Linq;
using CarRentalProjectKM.BusinessLayer.Dtos;
using CarRentalProjectKM.BusinessLayer.Mappers;
using CarRentalProjectKM.DataLayer.Repositories;

namespace CarRentalProjectKM.BusinessLayer.Services
{
    public class CarService
    {
        public bool AddSpecyficCar(CarDto carDto)
        {
            var car = DtoToEntityMappers.CarDtoToEntityModel(carDto);
            return CarRepository.AddSpecyficCar(car);
        }
        
        public List<CarModelDto> GettAllCarModels()
        {
            var allCarModels = new List<CarModelDto>();

            var carModelRepository = new CarModelRepository();
            var carModels = carModelRepository.GetAllCarsModel();

            foreach (var carModel in carModels)
            {
                var carModelDto = EntityToDtoMapper.CarModelEntityModelToDto(carModel);
                allCarModels.Add(carModelDto);
            }
            return allCarModels;
        }

        public List<CarDto> GetAllCars()
        {
            var allCars = new List<CarDto>();
            var rentCarRepository = new RentCarRepository();
            foreach (var car in rentCarRepository.GetAllCars())
            {
                var carDto = EntityToDtoMapper.CarEntityModelToDto(car);
                allCars.Add(carDto);
            }
            return allCars;
        }
    }
}
