﻿using CarRentalProjectKM.BusinessLayer.Dtos;
using CarRentalProjectKM.BusinessLayer.Mappers;
using CarRentalProjectKM.DataLayer.Repositories;

namespace CarRentalProjectKM.BusinessLayer.Services
{
    public class CarModelService
    {
        public bool AddCarModel(CarModelDto carDto)
        {
            var car = DtoToEntityMappers.CarModelDtoToEntityModel(carDto);
            return CarModelRepository.AddCarModel(car);
        }
    }
}
