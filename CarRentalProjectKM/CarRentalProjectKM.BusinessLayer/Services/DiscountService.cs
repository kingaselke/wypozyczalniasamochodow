﻿using System.Collections.Generic;
using CarRentalProjectKM.BusinessLayer.Dtos;
using CarRentalProjectKM.BusinessLayer.Mappers;
using CarRentalProjectKM.DataLayer.Repositories;

namespace CarRentalProjectKM.BusinessLayer.Services
{
    public class DiscountService
    {
        public bool AddDiscount(DiscountDto discountDto)
        {
            var discount = DtoToEntityMappers.DiscountDtoToEntityModel(discountDto);
            return DiscountRepository.AddDiscount(discount);
        }

        public static List<DiscountDto> GettAllDiscounts()
        {
            var allDiscounts = new List<DiscountDto>();

            var discountRepository = new DiscountRepository();
            var discounts = discountRepository.GetAllDiscounts();

            foreach (var discount in discounts)
            {
                var discountDto = EntityToDtoMapper.DiscountEntityModelToDto(discount);
                allDiscounts.Add(discountDto);
            }
            return allDiscounts;
        }

        public double TotalPrice(double totalPrice, DiscountDto discountDto)
        {
            var ds = new DiscountRepository();
            var listOfDiscounts = ds.GetAllDiscounts();
            var totalCost = totalPrice - discountDto.DiscountPercentage * totalPrice / 100;

            return totalCost;
        }
    }
}
