﻿using System.Collections.Generic;
using CarRentalProjectKM.BusinessLayer.Dtos;
using CarRentalProjectKM.BusinessLayer.Mappers;
using CarRentalProjectKM.DataLayer.Repositories;

namespace CarRentalProjectKM.BusinessLayer.Services
{
    public class RentCarService
    {
        public List<CarDto> GetAvailableCars()
        {
            var availableCars = new List<CarDto>();
            var rentCarRepository = new RentCarRepository();
            var allCars = rentCarRepository.GetAllCars();
            foreach (var car in allCars)
            {
                if (!car.RentOrNotRent) continue;
                var carDto = EntityToDtoMapper.CarEntityModelToDto(car);
                availableCars.Add(carDto);
            }
            return availableCars;
        }

        public List<CustomerDto> GetAllCustomers()
        {
            var allCustomers = new List<CustomerDto>();
            var rentCarRepository = new RentCarRepository();
            var customers = rentCarRepository.GetAllCustomers();
            foreach (var customer in customers)
            {
                var customerDto = EntityToDtoMapper.CustomerEntityModelToDto(customer);
                allCustomers.Add(customerDto);
            }
            return allCustomers;
        }

        public bool AddRental(RentCarDto rentCarDto)
        {
            var rentCar = DtoToEntityMappers.RentCarDtoToEntityModel(rentCarDto);
            var rentCarRepository = new RentCarRepository();
            return rentCarRepository.AddRental(rentCar);
        }

        public List<RentCarDto> AllRentalByPesel(long pesel)
        {
            var allRentals = new List<RentCarDto>();
            var rentCarRepository = new RentCarRepository();
            var rentals = rentCarRepository.GetAllRentalsByPesel(pesel);
            foreach (var rent in rentals)
            {
                RentCarDto rentCarDto = EntityToDtoMapper.RentCarEntityModelToDto(rent);
                allRentals.Add(rentCarDto);
            }
            return allRentals;
        }
    }
}
