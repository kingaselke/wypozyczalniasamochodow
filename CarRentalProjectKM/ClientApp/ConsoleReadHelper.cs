﻿using System;

namespace ClientApp
{
    internal class ConsoleReadHelper
    {
        public static long GetLong(string message)
        {
            long pesel;
            Console.Write(message);

            while (!long.TryParse(Console.ReadLine(), out pesel))
            {
                Console.WriteLine("\nPodano błędną wartość - spróbuj ponownie!\n");
            }

            return pesel;
        }

        public static int GetInt(string message)
        {
            int number;
            Console.Write(message);

            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("\nWartość musi być liczbą - spróbuj ponownie!\n");
            }

            return number;
        }
    }
}
