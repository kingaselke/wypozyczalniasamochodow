﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalProjectKM.BusinessLayer.Dtos;
using CarRentalProjectKM.BusinessLayer.Services;
using CarRentalProjectKM.DataLayer.Repositories;

namespace ClientApp
{
    internal class ProgramLoop
    {
        public void Execute()
        {

            var customerService = new CustomerService();
            var client = new CustomerDto();
            var done = false;
            while (!done)
            {
                var pesel = ConsoleReadHelper.GetLong("Podaj PESEL aby się zalogować: \n");
                if (customerService.CheckIfCustomerExistsInTheSystemByPesel(pesel))
                {
                    client = customerService.GetCustomerByPesel(pesel);
                    Console.WriteLine("\nLogowanie przebiegło pomyślnie!!\n");

                    var choose = ConsoleReadHelper.GetInt("Co chcesz zrobić?\n" +
                                                          "1.Wyświetl historię wypożyczeń.\n" +
                                                          "2.Edytuj swoje dane.");
                    switch (choose)
                    {
                        case 1:
                            ShowRentHistory(pesel);
                            break;
                        case 2:
                            ChangePersonalData(pesel);
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Brak klienta o podanym peselu.Spróbuj ponownie.");
                }
            }
        }

        private void ShowRentHistory(long pesel)
        {
            var rentCarService = new RentCarService();
            var listOfRentals = rentCarService.AllRentalByPesel(pesel);
            Console.WriteLine("\n\nHistoria wypożyczeń:\n");
            int i = 1;
            foreach (var rent in listOfRentals)
            {
                Console.WriteLine(i++ + ".");
                Console.WriteLine("Data wypożyczenia: " + rent.RentDate);
                Console.WriteLine("Data zwrotu: " + rent.ReturnDate);
                Console.WriteLine("Model samochodu: " + rent.CarDto.CarModelDto.Brand + " " + rent.CarDto.CarModelDto.Model);
                Console.WriteLine("Cena za wynajem: " + rent.TotalPrice);
            }
        }

        private void ChangePersonalData(long pesel)
        {
            var customerService = new CustomerService();
            var customerDto = customerService.GetCustomerByPesel(pesel);

            CustomerDto newCustomerDto = new CustomerDto();
            Console.WriteLine("Wpisz nowe dane klienta lub pozostaw puste pole by pozostawić aktualne dane.\n");

            newCustomerDto.Pesel = customerDto.Pesel;

            Console.WriteLine("Imię klienta: " + customerDto.Name + "   Nowe imię klienta: ");
            var newName = Console.ReadLine();
            newCustomerDto.Name = string.IsNullOrWhiteSpace(newName) ? customerDto.Name : newName;

            Console.WriteLine("Nazwisko klienta: " + customerDto.Surname + "   Nowe nazwisko klienta: ");
            var newSurname = Console.ReadLine();
            newCustomerDto.Surname = string.IsNullOrWhiteSpace(newSurname) ? customerDto.Surname : newSurname;

            customerService.ChangeCustomerData(newCustomerDto);
        }
    }
}


//      
//        studentService.ChangeStudentData(newStudentDto);