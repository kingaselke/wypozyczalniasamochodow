﻿namespace CarRentalProjectKM.DataLayer.Models
{
    public class CarModel
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public double PricePerDay { get; set; }
    }
}
