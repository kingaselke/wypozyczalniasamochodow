﻿using System;

namespace CarRentalProjectKM.DataLayer.Models
{
    public class RentCar
    {
        public int Id { get; set; }
        public DateTime RentDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public Customer Customer { get; set; }
        public Car Car { get; set; }
        public double? TotalPrice { get; set; }
    }
}
