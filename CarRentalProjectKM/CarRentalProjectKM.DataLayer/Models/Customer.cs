﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRentalProjectKM.DataLayer.Models
{
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Pesel { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
