﻿namespace CarRentalProjectKM.DataLayer.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Plate { get; set; }
        public bool RentOrNotRent { get; set; }
        public CarModel CarModel { get; set; }
    }
}
