﻿namespace CarRentalProjectKM.DataLayer.Models
{
    public class Discount
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double DiscountPercentage { get; set; }
    }
}
