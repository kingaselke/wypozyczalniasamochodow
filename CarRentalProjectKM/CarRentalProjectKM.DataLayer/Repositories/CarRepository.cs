﻿using System.Linq;
using CarRentalProjectKM.DataLayer.dbContext;
using CarRentalProjectKM.DataLayer.Models;

namespace CarRentalProjectKM.DataLayer.Repositories
{
    public class CarRepository
    {
        public static bool AddSpecyficCar(Car car)
        {
            Car addedCar;

            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                dbContext.CarsDbSet.Attach(car);
                addedCar = dbContext.CarsDbSet.Add(car);
                dbContext.SaveChanges();
            }
            return addedCar != null;
        }

        public void ChangeRentOrNotRent(string plate)
        {
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                var car = dbContext.CarsDbSet.First(c => c.Plate == plate);
                car.RentOrNotRent = true;
                dbContext.SaveChanges();
            }
        }
    }
}