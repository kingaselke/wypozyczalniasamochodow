﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using CarRentalProjectKM.DataLayer.Models;
using CarRentalProjectKM.DataLayer.dbContext;

namespace CarRentalProjectKM.DataLayer.Repositories
{
    public class CustomersRepository
    {
        public List<Customer> GetCustomerByPesel(long pesel)
        {
            List<Customer> customer;

            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                customer = dbContext.CustomersDbSet.Where(b => b.Pesel == pesel).ToList();
            }
            return customer;
        }

        public bool AddCustomer(Customer customer)
        {
            Customer addedCustomer;

            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                addedCustomer = dbContext.CustomersDbSet.Add(customer);
                dbContext.SaveChanges();
            }

            return addedCustomer != null;
        }

        public void ChangeCustomerRepository(Customer customer)
        {
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                var specyficCustomer = dbContext.CustomersDbSet.First(x => x.Pesel == customer.Pesel);
                specyficCustomer.Name = customer.Name;
                specyficCustomer.Surname = customer.Surname;
                dbContext.SaveChanges();
            }
        }
    }
}
