﻿using System.Collections.Generic;
using System.Linq;
using CarRentalProjectKM.DataLayer.dbContext;
using CarRentalProjectKM.DataLayer.Models;

namespace CarRentalProjectKM.DataLayer.Repositories
{
    public class DiscountRepository
    {
        public static bool AddDiscount(Discount discount)
        {
            int rowsAffected;

            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                dbContext.DiscountDbSet.Add(discount);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected == 1;
        }
        public List<Discount> GetAllDiscounts()
        {
            List<Discount> discount;
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                discount = dbContext.DiscountDbSet.ToList();
            }
            return discount;
        }

        public static bool IsDiscountInDb(string name)
        {
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                var modelDb = dbContext.DiscountDbSet.Count(x => x.Name == name);
                return modelDb != 0;
            }
        }
    }
}