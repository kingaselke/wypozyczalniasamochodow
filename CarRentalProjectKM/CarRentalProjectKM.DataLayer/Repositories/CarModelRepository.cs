﻿using System.Collections.Generic;
using System.Linq;
using CarRentalProjectKM.DataLayer.dbContext;
using CarRentalProjectKM.DataLayer.Models;

namespace CarRentalProjectKM.DataLayer.Repositories
{
    public class CarModelRepository
    {
        public static bool AddCarModel(CarModel car)
        {
            int rowsAffected;

            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                dbContext.CarsModelDbSet.Add(car);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected == 1;
        }

        public List<CarModel> GetAllCarsModel()
        {
            List<CarModel> cars;
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                cars = dbContext.CarsModelDbSet.ToList();
            }
            return cars;
        }

        public static bool IsModelInDb(string model)
        {
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                var modelDb = dbContext.CarsModelDbSet.Count(x => x.Model == model);
                return modelDb != 0;
            }
        }
    }
}