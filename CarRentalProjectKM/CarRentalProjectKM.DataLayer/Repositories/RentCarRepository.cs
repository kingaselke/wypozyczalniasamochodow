﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using CarRentalProjectKM.DataLayer.dbContext;
using CarRentalProjectKM.DataLayer.Models;

namespace CarRentalProjectKM.DataLayer.Repositories
{
    public class RentCarRepository
    {
        public List<Car> GetAllCars()
        {
            List<Car> cars;
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                cars = dbContext.CarsDbSet.Include(c => c.CarModel).ToList();
            }

            return cars;
        }

        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers;
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                customers = dbContext.CustomersDbSet.ToList();
            }
            return customers;
        }

        public bool AddRental(RentCar rentCar)
        {
            RentCar addedRental;

            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                dbContext.RentCarDbSet.Attach(rentCar);
                addedRental = dbContext.RentCarDbSet.Add(rentCar);
                var manager = ((IObjectContextAdapter)dbContext).ObjectContext.ObjectStateManager;
                manager.ChangeObjectState(rentCar.Car, EntityState.Modified);
                dbContext.SaveChanges();
            }

            return addedRental != null;
        }

        public List<RentCar> GetAllRentalsByPlate(string plate)
        {
            List<RentCar> carRentals;
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                carRentals = dbContext.RentCarDbSet.Where(r => r.Car.Plate == plate).Include(c => c.Car).Include(cm => cm.Car.CarModel).Include(cu => cu.Customer).ToList();
            }
            return carRentals;
        }

        public List<RentCar> GetAllRentalsByPesel(long pesel)
        {
            List<RentCar> carRentalsByPerson;
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                carRentalsByPerson = dbContext.RentCarDbSet.Where(r => r.Customer.Pesel == pesel).Include(c => c.Car).Include(cm => cm.Car.CarModel).Include(cu => cu.Customer).ToList();
            }
            return carRentalsByPerson;
        }

        public void ChangeReturnDate(DateTime date, int id)
        {
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                var rentCar = dbContext.RentCarDbSet.First(r => r.Id == id);
                rentCar.ReturnDate = date;
                dbContext.SaveChanges();
            }
        }
        public void ChangeTotalPrice(double price, int id)
        {
            using (var dbContext = new CarRentalProjectKMDbContext())
            {
                var rentCar = dbContext.RentCarDbSet.First(r => r.Id == id);
                rentCar.TotalPrice = price;
                dbContext.SaveChanges();
            }
        }
    }
}