﻿using System.Configuration;
using System.Data.Entity;
using CarRentalProjectKM.DataLayer.Models;

namespace CarRentalProjectKM.DataLayer.dbContext
{
    internal class CarRentalProjectKMDbContext : DbContext
    {
        public CarRentalProjectKMDbContext() : base(GetConnectionString())
        { }

        public DbSet<CarModel> CarsModelDbSet { get; set; }
        public DbSet<Customer> CustomersDbSet { get; set; }
        public DbSet<Car> CarsDbSet { get; set; }
        public DbSet<RentCar> RentCarDbSet { get; set; }
        public DbSet<Discount> DiscountDbSet { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["CarRentalProjectKMDbContext"].ConnectionString;
        }
    }
}




